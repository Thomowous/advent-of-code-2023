use std::time::Instant;

fn main() {
    let now = Instant::now();

    let input: Vec<Vec<char>> = include_str!("../input.txt")
        .lines()
        .map(|line| line.chars().collect())
        .collect();

    part1(input.clone());
    part2(input.clone());

    let elapsed = now.elapsed();
    println!("Done in: {:?}", elapsed)
}

fn part1(input: Vec<Vec<char>>) {
    let mut found_part_numbers: Vec<u32> = vec![];

    for y in 0 .. input.len() {
        let mut current_part_number_length = 1;
        let mut symbol_found = false;
        for x in 0 .. input[y].len() {
            if !input[y][x].is_digit(10) {
                if symbol_found {
                    let mut buffer = String::new();
                    for i in (1 .. current_part_number_length).rev() {
                        buffer.push(input[y][x - i])
                    }
                    found_part_numbers.push(buffer.parse::<u32>().unwrap());
                    symbol_found = false;
                }
                current_part_number_length = 1;
                continue;
            }
            symbol_found |= has_symbol_adjacent(&input, y, x);
            current_part_number_length += 1;
        }

        if symbol_found {
            let mut buffer = String::new();
            for i in (1 .. current_part_number_length).rev() {
                buffer.push(input[y][input[y].len() - i])
            }
            found_part_numbers.push(buffer.parse::<u32>().unwrap());
        }
    }

    let sum: u32 = found_part_numbers.iter().sum();
    println!("Day 3 part 1 answer: {sum}");
}

fn part2(input: Vec<Vec<char>>) {
    let mut result = 0;
    for y in 0 .. input.len() {
        let mut positions = ((0, 0), (0, 0));
        for x in 0 .. input[y].len() {
            if input[y][x] != '*' {
                continue;
            }
            positions = get_adjacent_number_positions(&input, y, x);
            if (positions.0 == (0, 0) || positions.1 == (0, 0)) {
                continue;
            }
            let num1 = get_number_from_position(&input, positions.0.0, positions.0.1);
            let num2 = get_number_from_position(&input, positions.1.0, positions.1.1);
            result += num1 * num2;
        }
    }
    // 78740604 high
    // 71612444 wrong
    // 65368245 low
    // 5198579 low
    println!("Day 3 part 2 answer: {result}");
}


fn get_char(input: &Vec<Vec<char>>, y: usize, y_offset: i32, x: usize, x_offset: i32) -> char {
    if y as i32 + y_offset < 0 || y as i32 + y_offset >= input.len() as i32 {
        return '.';
    }
    if x as i32 + x_offset < 0 || x as i32 + x_offset >= input[y].len() as i32 {
        return '.';
    }
    return input[(y as i32 + y_offset) as usize][(x as i32 + x_offset) as usize];
}

fn has_symbol_adjacent(input: &Vec<Vec<char>>, y: usize, x: usize) -> bool {
    for y_offset in -1 ..= 1 {
        for x_offset in -1 ..= 1 {
            let test_char = get_char(&input, y, y_offset, x, x_offset);
            if test_char == '.' || test_char.is_digit(10) {
                continue;
            }
            return true;
        }
    }
    return false;
}

fn get_adjacent_number_positions(input: &Vec<Vec<char>>, y: usize, x: usize) -> ((usize, usize), (usize, usize)) {
    let mut number1_position = (0, 0);
    let mut number2_position = (0, 0);
    for y_offset in -1 ..= 1 {
        let mut found_prev = false;
        for x_offset in -1 ..= 1 {
            let test_char = get_char(&input, y, y_offset, x, x_offset);
            if test_char.is_digit(10) {
                if number1_position == (0, 0) {
                    number1_position = ((y as i32 + y_offset) as usize, (x as i32 + x_offset) as usize);
                    found_prev = true;
                } else if !found_prev {
                    number2_position = ((y as i32 + y_offset) as usize, (x as i32 + x_offset) as usize);
                }
            } else {
                found_prev = false;
            }

        }

    }
    return (number1_position, number2_position);
}

fn get_number_from_position(input: &Vec<Vec<char>>, y: usize, x: usize) -> u32 {
    let mut x_offset = 1;
    let mut buffer = String::from(input[y][x]);
    loop {
        let char = get_char(&input, y, 0, x, x_offset);
        if !char.is_digit(10) {
            break;
        }
        buffer = format!("{}{}", buffer, char);
        x_offset += 1;
    }
    x_offset = -1;
    loop {
        let char = get_char(&input, y, 0, x, x_offset);
        if !char.is_digit(10) {
            break;
        }
        buffer = format!("{}{}", char, buffer);
        x_offset -= 1;
    }

    return buffer.parse::<u32>().unwrap();
}
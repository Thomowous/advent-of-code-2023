use itertools::Itertools;

#[derive(PartialEq)]
enum Color {
    None,
    Red,
    Green,
    Blue
}

struct Cube {
    color: Color,
    count: u32,
}
fn new_cube(color_str: &str, count_str: &str) -> Cube {
    let mut color = Color::None;
    if (color_str == "red") {
        color = Color::Red;
    }
    if (color_str == "green") {
        color = Color::Green;
    }
    if (color_str == "blue") {
        color = Color::Blue;
    }
    let count = count_str.parse::<u32>().unwrap();
    Cube { color, count }
}

fn main() {
    let input = include_str!("../input.txt")
        .lines()
        .map(|line| line.splitn(2, ":").next_tuple().unwrap())
        .map(|(game, cubes)| (
            game.trim_matches(|char: char| !char.is_numeric()).parse::<u32>().unwrap(),
            cubes.split(";").map(|set| set
                .split(",")
                .map(|it| new_cube(
                    it.trim_matches(|char: char| !char.is_alphabetic()),
                    it.trim_matches(|char: char| !char.is_numeric()))
                )
            )
        ));


    let part1: u32 = input.clone()
        .filter(|(game, sets)| sets.clone()
            .filter(|set| set.clone()
                .any(|cube| {
                    (cube.color == Color::Red && cube.count > 12) ||
                    (cube.color == Color::Green && cube.count > 13) ||
                    (cube.color == Color::Blue && cube.count > 14)
                })
            )
            .count() == 0
        )
        .map(|(game, _)| game)
        .sum();

    println!("Day 2 part 1 answer: {}", part1);


    let part2: u32 = input.clone()
        .map(|(_, sets)| sets.flatten())
        .map(|set| {
            let mut produce = 1;
            for color in [Color::Red, Color::Blue, Color::Green] {
                produce *= set.clone().max_by(|cube1, cube2| {
                    let cube1_count = if cube1.color == color { cube1.count } else { 0 };
                    let cube2_count = if cube2.color == color { cube2.count } else { 0 };
                    cube1_count.cmp(&cube2_count)
                }).unwrap().count;
            }
            return produce;
        })
        .sum();

    println!("Day 2 part 2 answer: {}", part2);
}

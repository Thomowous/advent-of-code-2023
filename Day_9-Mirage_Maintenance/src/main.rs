use std::time::Instant;

fn get_next_value(sequence: &Vec<i64>) -> i64 {
    if sequence.iter().all(|num| *num == 0) {
        return 0;
    }
    let mut last = sequence.last().unwrap();
    let differences = get_differences(sequence);
    let temp = get_next_value(&differences);
    return *last + temp;
}

fn get_prev_value(sequence: &Vec<i64>) -> i64 {
    let mut first = sequence.first().unwrap();
    if sequence.iter().all(|num| *num == 0) {
        return 0;
    }
    let differences = get_differences(sequence);
    let temp = get_prev_value(&differences);
    return *first - temp;
}

fn get_differences(sequence: &Vec<i64>) -> Vec<i64> {
    let mut differences = vec![0; sequence.len() - 1];
    for i in 0 .. sequence.len() - 1 {
        differences[i] = sequence[i + 1] - sequence[i];
    }
    return differences;
}

fn main() {
    let now = Instant::now();

    let input = include_str!("../input.txt")
        .lines()
        .map(|line| line
            .split_whitespace()
            .map(|num| num.parse::<i64>().unwrap())
            .collect::<Vec<i64>>()
        );

    let part1: i64 = input.clone()
        .map(|seq| get_next_value(&seq))
        .sum();
    println!("Day 9 part 1 answer: {part1}");

    let part2: i64 = input
        .map(|seq| get_prev_value(&seq))
        .sum();
    println!("Day 9 part 2 answer: {part2}");

    let elapsed = now.elapsed();
    println!("done in {:?}", elapsed)
}

use std::collections::HashSet;
use std::time::Instant;
use itertools::Itertools;

fn main() {
    let now = Instant::now();

    let data = include_str!("../input.txt")
        .lines()
        .map(|line| line.split(": ").nth(1).unwrap())
        .map(|line| line.split(" | ").next_tuple().unwrap())
        .map(|(winning_numbers, my_numbers)|
            (
                winning_numbers.split(" ")
                    .filter(|i| !i.is_empty())
                    .collect::<HashSet<_>>(),
                my_numbers.split(" ")
                    .filter(|i| !i.is_empty())
            )
        )
        .map(|(winning_numbers, my_numbers)|
            my_numbers.filter(|i| winning_numbers.contains(i)).count(),
        )
        .collect::<Vec<_>>();


    let mut part1 = 0;
    for count in &data {
        if *count >= 1 {
            part1 += 1 << (count - 1);
        }
    }

    println!("Day 4 part 1 answer: {part1}");


    let mut card_nums = vec![1; data.len()];

    let mut part2 = 0;
    for (index, count) in data.iter().enumerate() {
        for x in 1 ..= *count {
            card_nums[index + x] += card_nums[index];
        }
        part2 += card_nums[index];
    }
    println!("Day 4 part 2 answer: {part2}");

    let elapsed = now.elapsed();
    println!("done in {:?}", elapsed);

}

use std::ops::Range;
use std::time::Instant;
use itertools;
use itertools::{Itertools, max, min};

fn main() {
    let now = Instant::now();

    let input = include_str!("../rada_input.txt");

    let mut seeds1 = input
        .lines()
        .next()
        .unwrap()
        .trim_matches(|char: char| !char.is_numeric())
        .split_whitespace()
        .map(|seed|
             vec![
                seed.parse::<usize>().unwrap(), // seed
                0, // soil
                0, // fertilizer
                0, // water
                0, // light
                0, // temperature
                0, // humidity
                0, // location
            ]
        )
        .collect::<Vec<Vec<_>>>();




    input
        .lines()
        .skip(1)
        .group_by(|line| line.is_empty())
        .into_iter()
        .filter(|(b, _)| !b)
        .map(|(_, group)| group
            .map(|line| line.trim_matches(|char: char| !char.is_numeric()))
            .filter(|line| line.len() > 0)
            .map(|line| line
                .split_whitespace()
                .map(|num| num.parse::<usize>().unwrap())
                .next_tuple::<(_, _, _)>()
                .unwrap()
            )
        )
        .enumerate()
        .for_each(|(i, group)| {
            group.for_each(|(dest, src, length)| {
                for seed in &mut seeds1 {
                    if seed[i] < src || seed[i] >= (src + length) {
                        continue;
                    }
                    seed[i + 1] = dest + (seed[i] - src);
                }

            });

            for seed in &mut seeds1 {
                if seed[i + 1] == 0 {
                    seed[i + 1] = seed[i];
                }
            }
        });



    let part1 = seeds1.iter().min_by(|seed1, seed2| seed1[7].cmp(&seed2[7])).unwrap()[7];
    println!("{part1}");


    let mut seeds2 = input
        .lines()
        .next()
        .unwrap()
        .trim_matches(|char: char| !char.is_numeric())
        .split_whitespace()
        .chunks(2)
        .into_iter()
        .map(|mut chunk| chunk.next_tuple().unwrap())
        .map(|(seed_start, seed_length)| {
            let seed_start_parsed = seed_start.parse::<usize>().unwrap();
            let seed_length_parsed = seed_length.parse::<usize>().unwrap();
            vec![
                seed_start_parsed .. seed_start_parsed + seed_length_parsed, // seed
                0 .. 0, // soil
                0 .. 0, // fertilizer
                0 .. 0, // water
                0 .. 0, // light
                0 .. 0, // temperature
                0 .. 0, // humidity
                0 .. 0, // location
            ]
        })
        .collect::<Vec<Vec<_>>>();

    input
        .lines()
        .skip(1)
        .group_by(|line| line.is_empty())
        .into_iter()
        .filter(|(b, _)| !b)
        .map(|(_, group)| group
            .map(|line| line.trim_matches(|char: char| !char.is_numeric()))
            .filter(|line| line.len() > 0)
            .map(|line| line
                .split_whitespace()
                .map(|num| num.parse::<usize>().unwrap())
                .next_tuple::<(_, _, _)>()
                .unwrap()
            )
            .map(|(dest, src, length )|
                (
                    dest .. dest + length,
                    src .. src + length
                )
            )
        )
        .enumerate()
        .for_each(|(i, group)| {
            // println!("group {i}: {}", seeds2.len());
            let len = seeds2.len();
            group.for_each(|(dest_range, src_range)| {
                let mut j = 0;

                loop {
                    if j >= seeds2.len() {
                        break;
                    }
                    let my_seed = &seeds2[j][i];


                    if seeds2[j][i].start == 0 && seeds2[j][i].end == 0 {
                        j += 1;
                        continue;
                    }
                    let test_range = intersect(&seeds2[j][i], &src_range);
                    if test_range.start >= test_range.end {
                        j += 1;
                        continue;
                    }



                    let start_diff = test_range.start - src_range.start;
                    let new_range = dest_range.start + start_diff .. dest_range.start + start_diff + (test_range.end - test_range.start);

                    let mut new_vec = vec![0 .. 0; 8];
                    new_vec[i + 1] = new_range.clone();
                    seeds2.push(new_vec);
                    if new_range.start == 151140540 {
                        println!("asd");
                    }

                    let subtracted_ranges = subtract(&seeds2[j][i], &test_range);

                    if subtracted_ranges.0.start != subtracted_ranges.0.end {
                        if subtracted_ranges.0.start == 151140540 {
                            println!("asd");
                        }
                        let mut new_vec = vec![0 .. 0; 8];
                        new_vec[i + 1] = subtracted_ranges.clone().0;
                        seeds2.push(new_vec);
                    }
                    if subtracted_ranges.1.start != subtracted_ranges.1.end {
                        if subtracted_ranges.1.start == 151140540 {
                            println!("asd");
                        }
                        let mut new_vec = vec![0 .. 0; 8];
                        new_vec[i + 1] = subtracted_ranges.clone().1;
                        seeds2.push(new_vec);
                    }
                    seeds2[j][i] = 0 .. 0;
                    j += 1;
                }

            });
            let mut j = 0;
            loop {
                if j >= seeds2.len() {
                    break;
                }
                if seeds2[j][i].start == 0 && seeds2[j][i].end == 0 {
                    j += 1;
                    continue;
                }

                seeds2[j][i + 1] = seeds2[j][i].clone();
                j += 1;
            }

        });

    for seed in &seeds2 {
        if seed[7].start == 151140540 {
            println!("asd");
        }
    }

    let part2 = &seeds2.iter()
        .filter(|it| it[7].start != 0)
        .min_by(|seed1, seed2| seed1[7].start.cmp(&seed2[7].start)).unwrap()[7];
    println!("{}", part2.start);

    // 311913594 high
    // 151140540 high
    // 1209828   low


    // rada:
    // 28580589 correct
    // 414542691

    let elapsed = now.elapsed();
    println!("elapsed: {:?}", elapsed);
}



fn intersect(a: &Range<usize>, b: &Range<usize>) -> Range<usize> {
    a.start.max(b.start) .. a.end.min(b.end)
}

fn subtract(a: &Range<usize>, b: &Range<usize>) -> (Range<usize>, Range<usize>) {
    (a.start .. b.start, b.end .. a.end)
}
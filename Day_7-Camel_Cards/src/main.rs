use std::cmp::Ordering;
use std::time::Instant;
use itertools::Itertools;

const CARDS_PART1: &str = "AKQJT98765432";
const CARDS_PART2: &str = "AKQT98765432J";

struct Hand {
    string: String,
    cards: Vec<(char, usize)>,
    bid: usize,
}

fn hand_to_cards(hand: &str) -> Vec<(char, usize)> {
    hand.chars()
        .counts()
        .into_iter()
        .sorted_by(|(char1, count1), (char2, count2)| count1.cmp(count2))
        .rev()
        .collect()
}

fn convert_jokes(cards: &mut Vec<(char, usize)>) {
    let joker_count = match cards.iter().find(|(char, count)| *char == 'J') {
        Some(x) => x.1,
        _ => 0,
    };
    cards.retain(|(char, count)| *char != 'J');
    if cards.len() == 0 {
        cards.push(('J', 0));
    }
    cards[0].1 += joker_count;
}

fn get_score(cards: &Vec<(char, usize)>) -> usize {
    if cards[0].1 == 5 { // five of a kind
        return 6;
    }
    if cards[0].1 == 4 { // four of a kind
        return 5;
    }
    if cards[0].1 == 3 && cards[1].1 == 2 { // full house
        return 4;
    }
    if cards[0].1 == 3 { // three of a kind
        return 3;
    }
    if cards[0].1 == 2 && cards[1].1 == 2 { // two pairs
        return 2;
    }
    if cards[0].1 == 2 { // one pair
        return 1;
    }
    // high card
    return 0;
}

fn compare(first: &Hand, second: &Hand, part2: bool) -> Ordering {
    let hand1 = &first.string;
    let cards1 = &first.cards;

    let hand2 = &second.string;
    let cards2 = &second.cards;

    let mut score1 = get_score(cards1);
    let mut score2 = get_score(cards2);

    if score1 > score2 {
        return Ordering::Greater;
    }
    if score1 < score2 {
        return Ordering::Less;
    }

    let chars1 = hand1.chars().collect::<Vec<char>>();
    let chars2 = hand2.chars().collect::<Vec<char>>();
    for i in 0 .. 5 {
        let cards_string = if part2 { CARDS_PART2 } else { CARDS_PART1 };
        score1 = cards_string.len() - cards_string.find(chars1[i]).unwrap();
        score2 = cards_string.len() - cards_string.find(chars2[i]).unwrap();

        if score1 > score2 {
            return Ordering::Greater;
        }
        if score1 < score2 {
            return Ordering::Less
        }
    }
    return Ordering::Equal;
}

fn main() {
    let now = Instant::now();

    let input = include_str!("../input.txt")
        .lines()
        .map(|line| line.split_whitespace().next_tuple::<(_, _)>().unwrap())
        .map(|(hand, bid)|
            Hand {
                string: hand.to_string(),
                cards: hand_to_cards(hand),
                bid: bid.parse::<usize>().unwrap(),
            }
        );

    let part1 = input.clone()
        .sorted_by(|first, second| compare(first, second, false))
        .enumerate()
        .fold(0, |total, (rank, hand)| total + (rank + 1) * hand.bid);

    println!("Day 7 part 1 answer: {part1}");

    let part2 = input
        .map(|mut hand| {
            convert_jokes(&mut hand.cards);
            Hand {
                string: hand.string,
                cards: hand.cards,
                bid: hand.bid,
            }
        })
        .sorted_by(|first, second| compare(&first, &second, true))
        .enumerate()
        .fold(0, |total, (rank, hand)| total + (rank + 1) * hand.bid);

    println!("Day 7 part 2 answer: {part2}");

    let elapsed = now.elapsed();
    println!("done in {:?}", elapsed);
}

fn main() {
    let part1: u32 = include_str!("../input.txt")
        .lines()
        .map(|it| it.trim_matches(char::is_alphabetic))
        .map(|it| format!("{}{}", &it[.. 1], &it[it.len() - 1 ..]))
        .map(|it| it.parse::<u32>().unwrap())
        .sum();

    println!("Day 1 part 1 answer: {part1}");


    let digits = [
        ("one", "one1one"),
        ("two", "two2two"),
        ("three", "three3three"),
        ("four", "four4four"),
        ("five", "five5five"),
        ("six", "six6six"),
        ("seven", "seven7seven"),
        ("eight", "eight8eight"),
        ("nine", "nine9nine"),
    ];

    let part2: u32 = include_str!("../input.txt")
        .lines()
        .map(|it| it.to_string())
        .map(|mut str| {
            for (from, to) in digits {
                str = str.replace(from, to);
            }
            return str;
        })
        .map(|it| it.trim_matches(char::is_alphabetic).to_owned())
        .map(|it| format!("{}{}", &it[.. 1], &it[it.len() - 1 ..]))
        .map(|it| it.parse::<u32>().unwrap())
        .sum();

    println!("Day 1 part 2 answer: {part2}");
}

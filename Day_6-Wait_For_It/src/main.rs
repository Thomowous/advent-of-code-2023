use std::time::Instant;

fn main() {
    let now = Instant::now();

    let part1_input = include_str!("../input.txt")
        .lines()
        .map(|line| line
            .split(|char: char| !char.is_numeric())
            .filter(|it| !it.is_empty())
            .map(|num| num.parse::<u32>().unwrap())
            .collect::<Vec<_>>()
        )
        .collect::<Vec<Vec<_>>>();

    let mut part1 = 1;

    for i in 0 .. part1_input[0].len() {
        let time = part1_input[0][i];
        let record = part1_input[1][i];

        let mut wins = 0;

        for button_duration in 1 .. time {
            let remaining_time = time - button_duration;
            let distance = remaining_time * button_duration;
            if distance > record {
                wins += 1;
            }
        }
        part1 *= wins;
    }

    println!("{part1}");


    let part2_input = include_str!("../input.txt")
        .lines()
        .map(|line| line
            .split_whitespace()
            .skip(1)
            .collect::<String>()
            .parse::<u64>()
            .unwrap()
        )
        .collect::<Vec<_>>();



    let time = part2_input[0];
    let record = part2_input[1];

    let mut part2 = 0;

    for button_duration in 1 .. time {
        if (time - button_duration) * button_duration > record {
            part2 += 1;
        }
    }

    println!("{part2}");

    let elased = now.elapsed();
    println!("{:?}", elased)
}

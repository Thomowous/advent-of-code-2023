use std::collections::HashMap;
use std::time::Instant;
use num::integer::lcm;

fn main() {
    let now = Instant::now();

    let mut input = include_str!("../input.txt").lines();

    let instructions = input.clone().next().unwrap().chars();

    let mut nodes = input
        .skip(2)
        .map(|line| {
            let mut split_line = line
                .split(|char: char| !char.is_alphanumeric())
                .filter(|it| !it.is_empty());
            (
                split_line.next().unwrap(),
                HashMap::from([
                    ('L', split_line.next().unwrap()),
                    ('R', split_line.next().unwrap()),
                ]),
            )
        })
        .collect::<HashMap<_, HashMap<_, _>>>();

    let mut current_node = "AAA";
    let mut count = 0;
    for direction in instructions.clone().cycle() {
        if current_node == "ZZZ" {
            break;
        }
        current_node = nodes[current_node][&direction];
        count += 1;
    }

    println!("Day 8 part 1 answer: {count}");



    let mut current_nodes: Vec<&str> = nodes
        .iter()
        .filter(|(key, _)| key.ends_with("A"))
        .map(|(key, _)| *key)
        .collect();

    let mut counts = vec![0; current_nodes.len()];

    for i in 0 .. current_nodes.len() {
        for direction in instructions.clone().cycle() {
            if current_nodes[i].ends_with("Z") {
                break;
            }
            current_nodes[i] = nodes[current_nodes[i]][&direction];
            counts[i] += 1;
        }
    }

    let mut part2: u64 = counts[0];
    for count in counts {
        part2 = lcm(part2, count);
    }
    println!("Day 8 part 2 answer: {part2}");

    let elapsed = now.elapsed();
    println!("done in {:?}", elapsed);
}

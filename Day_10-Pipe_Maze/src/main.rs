use std::collections::{HashMap, LinkedList};
use std::ops::Add;
use crate::Direction::{Down, Left, Right, Up};

#[derive(Clone, Debug, Eq, PartialEq, Copy)]
struct Position {
    x: i32,
    y: i32
}

impl Position {
    pub fn new(x: i32, y: i32) -> Self {
        Position { x, y }
    }
}
impl Add<Position> for Position {
    type Output = Position;

    fn add(self, rhs: Position) -> Self::Output {
        Position::new(self.x + rhs.x, self.y + rhs.y)
    }
}

#[derive(Clone, Debug)]
struct Pipe {
    position: Position,
    piece: char,
    distance: u32,
}

#[derive(PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn main() {
    let input = include_str!("../input.txt")
        .lines()
        .map(|line| line.chars().collect())
        .collect::<Vec<Vec<char>>>();

    let starting_position_y = input.iter().position(|line| line.contains(&'S')).unwrap();
    let starting_position_x = input[starting_position_y].iter().position(|char| char == &'S').unwrap();
    let starting_pipe = Pipe {
        position: Position::new(starting_position_x as i32, starting_position_y as i32),
        piece: 'S',
        distance: 0
    };


    let directions = HashMap::from([
        (Up, Position::new(0, -1)),
        (Down, Position::new(0, 1)),
        (Left, Position::new(-1, 0)),
        (Right, Position::new(1, 0)),
    ]);

    let pipe_map = HashMap::from([
        ('|', vec![directions[&Up], directions[&Down]]),
        ('-', vec![directions[&Left], directions[&Right]]),
        ('L', vec![directions[&Up], directions[&Right]]),
        ('J', vec![directions[&Up], directions[&Left]]),
        ('7', vec![directions[&Down], directions[&Left]]),
        ('F', vec![directions[&Down], directions[&Right]]),
        ('S', vec![directions[&Up], /* directions[&Down], */ directions[&Left], /* directions[&Right] */]), // hardcoded :EHEHE:
    ]);


    let mut visited: Vec<Pipe> = Vec::new();

    let mut queue: LinkedList<Pipe> = LinkedList::new();
    queue.push_back(starting_pipe);

    while !queue.is_empty() {
        let pipe = queue.pop_front().unwrap();
        let directions = pipe_map[&pipe.piece].clone();

        for direction in directions {
            let new_position = pipe.clone().position + direction;
            if new_position.x < 0 {
                continue;
            }
            let new_piece = input[new_position.y as usize][new_position.x as usize];
            if new_piece == '.' {
                continue;
            }
            let v = visited.iter().find(|pos| pos.position == new_position);
            match v {
                None => {},
                _ => continue
            };

            let new_pipe = Pipe {
                piece: input[new_position.y as usize][new_position.x as usize],
                distance: pipe.distance + 1,
                position: new_position,
            };
            queue.push_back(new_pipe);
        }

        visited.push(pipe);
    }

    println!("Day 10 part 1 answer: {}", visited.iter().max_by(|pipe1, pipe2| pipe1.distance.cmp(&pipe2.distance)).unwrap().distance);
}

